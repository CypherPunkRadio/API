import asyncio
import os

import uvicorn
from api.main import app
from api.dl_queue import DL_Queue


def _setup_database():
    from api.models import Song
    Song.create_table()


if __name__ == '__main__':
    _setup_database()
    asyncio.run(DL_Queue.start_worker())
    os.system('mpc play')
    uvicorn.run(app, host='0.0.0.0', port=8080)
