import os

import aiohttp
import pytube
from pytube import YouTube

import settings
from api.models import Song, db


class UTILS:

    @staticmethod
    async def get_listeners():
        async with aiohttp.ClientSession() as session:
            async with session.get(f'http://{settings.ICECAST_HOST}:{settings.ICECAST_POST}/status-json.xsl') as resp:
                listeners: dict = await resp.json()
                # TODO peak listeners
                listeners = listeners.get('icestats').get('source').get('listeners')
                return listeners

    @staticmethod
    async def get_song():
        from mpd import MPDClient

        client = MPDClient()
        client.connect(settings.MAIN_RADIO_HOST, settings.MAIN_RADIO_PORT)

        song = client.currentsong().get('title', None)

        client.disconnect()

        return song

    @staticmethod
    def download_song(song_id):
        os.chdir(settings.MUSIC_PATH)

        try:
            song = YouTube(f'https://www.youtube.com/watch?v={song_id}')
        except Exception:
            raise Exception('Invalid ID!')

        title = pytube.helpers.safe_filename(song.title)
        artist = song.author

        song.streams.get_audio_only().download(filename=f'{song.video_id}')

        os.system(f"ffmpeg -i '{song.video_id}' -qscale 0 '{song.video_id}.ogg'")
        os.system(f"rm '{song.video_id}'")
        os.system(f"vorbiscomment -a '{song.video_id}.ogg' -t 'artist={artist}' -t 'title={title}' >/dev/null")

        os.system('mpc update >/dev/null')
        os.system(f'mpc add {song.video_id}.ogg >/dev/null')

    @staticmethod
    async def delete_song(song_id):
        os.chdir(settings.MUSIC_PATH)

        try:
            with db.atomic():
                Song.delete().where(Song.id == song_id).execute()
            os.system(f'rm {song_id}.ogg')
        except Exception as e:
            raise e

        return True
