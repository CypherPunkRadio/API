from fastapi import APIRouter, status
from fastapi.responses import JSONResponse

from api.models import Song, pw
from api.utils import UTILS

router = APIRouter(prefix='/api')


@router.get('/all')
async def get_all():
    songs: dict = {}

    for s in Song.select():
        songs.update({s.id: {'added': str(s.date), 'title': s.title}})

    return JSONResponse(content=songs)


@router.get('/song')
async def get_song():
    response: dict = {}

    song = await UTILS.get_song()

    try:
        s: Song = Song.select().where(Song.title.contains(song)).get()
        response.update({'current_song': {'title': song, 'ID': s.id, 'added': str(s.date)}})
    except AttributeError:
        response.update({'current_song': {'title': None}})

    return JSONResponse(content=response)


@router.get('/listeners')
async def get_listeners():
    response: dict = {}

    try:
        listeners = await UTILS.get_listeners()
        response.update({'listeners': listeners})
    except AttributeError:
        response.update({'listeners': 0})

    return JSONResponse(content=response)


@router.get('/search/{title}')
async def search_song(title: str):
    response: dict = {}

    try:
        for s in Song.select().where(Song.title.contains(title)):
            response.update({s.id: {'added': str(s.date), 'title': s.title}})
    except pw.DoesNotExist:
        return JSONResponse(status_code=status.HTTP_404_NOT_FOUND, content=response)

    return JSONResponse(status_code=status.HTTP_302_FOUND, content=response)
