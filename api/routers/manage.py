import pytube
from pytube import exceptions
from fastapi import APIRouter, status, Request
from fastapi.responses import JSONResponse

import settings

from api.models import pw, Song, db
from api.dl_queue import DL_Queue
from api.utils import UTILS
from api import limiter

router = APIRouter(prefix='/api/manage')


@router.put('/{secret}/add/{song_id}')
@limiter.limit('10/second')
async def add_song(secret: str, song_id: str, request: Request):
    response: dict = {}

    if secret != settings.API_SECRET:
        response.update({'error': {418: 'Invalid secret!'}})
        return JSONResponse(status_code=status.HTTP_418_IM_A_TEAPOT, content=response)

    if not song_id:
        response.update({'error': {400: 'Empty ID!'}})
        return JSONResponse(status_code=status.HTTP_400_BAD_REQUEST, content=response)
    elif len(song_id) != 11:
        response.update({'error': {422: 'Invalid ID!'}})
        return JSONResponse(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, content=response)

    video = pytube.YouTube(f'https://www.youtube.com/watch?v={song_id}')

    try:
        title = video.title
    except pytube.exceptions.VideoUnavailable:
        response.update({'error': {422: 'Invalid ID!'}})
        return JSONResponse(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, content=response)

    try:
        song = Song.select().where(Song.id == song_id).get()
        response.update({'error': {409: 'Song already exists!'}})
        return JSONResponse(status_code=status.HTTP_409_CONFLICT, content=response)
    except pw.DoesNotExist:
        pass

    try:
        await DL_Queue.add(song_id)
        title = pytube.helpers.safe_filename(video.title)
        with db.atomic():
            Song.create(id=song_id, title=title)
    except Exception as e:
        if str(e) == 'Invalid ID!':
            response.update({'error': {422: str(e)}})
            return JSONResponse(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, content=response)
        response.update({'error': {500: 'Download error!'}})
        return JSONResponse(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, content=response)

    response.update({song_id: {'added': True, 'title': title}})
    return JSONResponse(status_code=status.HTTP_201_CREATED, content=response)


@router.delete('/{secret}/delete/{song_id}')
async def delete_song(secret: str, song_id: str):
    response: dict = {}

    if secret != settings.API_SECRET:
        response.update({'error': {418: 'Invalid secret!'}})
        return JSONResponse(status_code=status.HTTP_418_IM_A_TEAPOT, content=response)

    if not song_id:
        response.update({'error': {400: 'Empty ID!'}})
        return JSONResponse(status_code=status.HTTP_400_BAD_REQUEST, content=response)
    elif 11 < len(song_id):
        response.update({'error': {422: 'Invalid ID!'}})
        return JSONResponse(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, content=response)

    try:
        song = Song.select().where(Song.id == song_id).get()
    except pw.DoesNotExist:
        response.update({'error': {404: "Song doesn't exist!"}})
        return JSONResponse(status_code=status.HTTP_404_NOT_FOUND, content=response)

    try:
        await UTILS.delete_song(song_id)
    except Exception:
        response.update({'error': {500: "Internal server error!"}})
        return JSONResponse(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, content=response)

    response.update({'deleted': True})
    return JSONResponse(status_code=status.HTTP_200_OK, content=response)
