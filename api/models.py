import peewee as pw
from playhouse.db_url import PooledMySQLDatabase

import settings

db = PooledMySQLDatabase(settings.DB_NAME, user=settings.DB_USER, password=settings.DB_PASSWORD,
                         host=settings.DB_HOST, port=settings.DB_PASSWORD)


class Song(pw.Model):
    id = pw.CharField(primary_key=True, unique=True)
    title = pw.CharField(null=False)
    date = pw.DateTimeField(constraints=[pw.SQL('DEFAULT CURRENT_TIMESTAMP')])

    class Meta:
        database = db
