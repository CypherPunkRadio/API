import queue
import threading

from api.utils import UTILS


class DL_Queue:
    __queue: queue.Queue = queue.Queue()

    @classmethod
    async def add(cls, song_id):
        cls.__queue.put(song_id)
        return True

    @classmethod
    def worker(cls):
        while True:
            song_id = cls.__queue.get()
            UTILS.download_song(song_id)

    @classmethod
    async def start_worker(cls):
        threading.Thread(target=DL_Queue.worker).start()
        threading.Thread(target=DL_Queue.worker).start()
        threading.Thread(target=DL_Queue.worker).start()
