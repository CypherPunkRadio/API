import os
import secrets

from dotenv import load_dotenv

load_dotenv()

cwd = os.path.dirname(os.path.realpath(__file__))

HOST = os.environ.get('HOST')
PORT = os.environ.get('PORT')

MAIN_RADIO_HOST = os.environ.get('MAIN_RADIO_HOST')
MAIN_RADIO_PORT = os.environ.get('MAIN_RADIO_PORT')

ICECAST_HOST = os.environ.get('ICECAST_HOST')
ICECAST_POST = os.environ.get('ICECAST_POST')

DB_PATH = os.path.join(cwd, 'db.sqlite3')

SECRET = os.environ.get('SECRET', secrets.token_hex(16))

API_SECRET = os.environ.get('API_SECRET')

MUSIC_PATH = os.environ.get('MUSIC_PATH')

DB_NAME = os.environ.get('DB_NAME')
DB_USER = os.environ.get('DB_USER')
DB_PASSWORD = os.environ.get('DB_PASSWORD')
DB_HOST = os.environ.get('DB_HOST')
DB_PORT = os.environ.get('DB_PORT')